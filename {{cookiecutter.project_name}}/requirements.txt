PyYAML==5.1
fastapi==0.33.0
SQLAlchemy==1.3.6
alembic==1.0.11
psycopg2-binary==2.8.3
uvicorn==0.8.4