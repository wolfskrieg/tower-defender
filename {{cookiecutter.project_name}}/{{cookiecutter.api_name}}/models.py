from pydantic import BaseModel
from typing import Optional


# ---- Example models for input request and output response
class SomeData(BaseModel):
    id: str
    name: str


class InputExample(BaseModel):
    example_int: int
    example_int_with_default: int = 0
    example_data: SomeData
    example_nullable_data: Optional[SomeData]


class OutputInteger(BaseModel):
    data: int


class OutputDefender(BaseModel):

    # Use this to return pydantic model from SQLAlchemy model
    class Config:
        orm_mode = True
