from random import randint

from sqlalchemy.orm import Session

from fastapi import APIRouter
from fastapi import Depends

import core.orm.models as models

from .dependencies import db
from .models import OutputDefender


router = APIRouter()


# Example no db session
@router.get('/', summary="Get some random integers")
def list_random_integers():
    """
    Returns the list of random integers
    """
    return [randint(0, 100) for _ in range(100)]


# Example with db session
@router.get('/', summary="Get some random integers")
def list_defenders_example(*, sess: Session = Depends(db)):
    """
    Returns the list of defenders in example
    """
    defenders = sess.query(models.Defender).all()

    return [OutputDefender.from_orm(defender) for defender in defenders]
