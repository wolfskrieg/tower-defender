# coding: utf-8

# ---- Imports for ease of use while writing task
# from sqlalchemy import BigInteger, Boolean, CHAR, Column, DateTime, Date, ForeignKey, ForeignKeyConstraint, \
#    Integer, String, Table, Text, UniqueConstraint, Numeric, text, Enum


# ---- Import if you are going to be using postgresql JSONB type
# from sqlalchemy.dialects.postgresql import JSONB

# Needed for eager loading of joined tables
from sqlalchemy.orm import relationship

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Defender(Base):
    __tablename__ = 'defenders'


class Tower(Base):
    __tablename__ = 'towers'


class Round(Base):
    __tablename__ = 'towers'
